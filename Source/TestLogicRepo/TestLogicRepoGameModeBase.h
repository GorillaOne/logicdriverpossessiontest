// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestLogicRepoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTLOGICREPO_API ATestLogicRepoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
