// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestLogicRepo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TestLogicRepo, "TestLogicRepo" );
